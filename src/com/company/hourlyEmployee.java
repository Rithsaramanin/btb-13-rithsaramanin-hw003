package com.company;

//====================>> Hourly-Employee <<====================
class hourlyEmployee extends staffMember{
    private int hoursWorked;
    private double rate;

    //  Getter
    public int getHour(){
        return hoursWorked;
    }
    public double getRate(){
        return rate;
    }
    //  Setter
    public void setHour(int h){
        hoursWorked = h;
    }
    public void setRate(double r){
        rate = r;
    }

    hourlyEmployee(int id, String name, String address,int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    public void ToString(){
        System.out.println("ID      : " + getID());
        System.out.println("Name    : " + getName());
        System.out.println("Address : " + getAddress());
        System.out.println("Hours   : " + getHour());
        System.out.println("Rate    : " + getRate());
        System.out.println("Payment : " + pay());
    }
    @Override
    public double pay() {
        double payment = hoursWorked * rate;
        return payment;
    }
}