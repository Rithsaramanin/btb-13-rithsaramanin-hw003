package com.company;

import java.util.*;
import java.util.regex.Pattern;


public class Main{
    Scanner sc = new Scanner(System.in);
    ArrayList<staffMember> staffMem;
    int iD,hourS,opt,remid,editid;
    String namE,addresS;
    double salarY,bonuS,ratE;

    Main(){
        staffMem = new ArrayList<staffMember>();

        staffMem.add(new Volunteer(1,"Roth","Phnom Penh"));
        staffMem.add(new salariedEmployee(2,"Nin","Phnom Penh",300,120));
        staffMem.add(new hourlyEmployee(3,"Koko","Takeo",12,50));
    }

    public static Comparator<staffMember> sNameComparator = new Comparator<staffMember>() {
        @Override
        public int compare(staffMember s1, staffMember s2) {
            String stuName1 = s1.name.toUpperCase();
            String stuName2 = s2.name.toUpperCase();

            return stuName1.compareTo(stuName2);
        }
    };

    public void show(){
        Collections.sort(staffMem,Main.sNameComparator);
        for (staffMember i: staffMem){
            System.out.println();
            i.ToString();
            System.out.println("==============================");
        }
    }

    public boolean regex(String s) {
        return Pattern.matches("\\D+", s);
    }
    public boolean regex1(String s) {
        return Pattern.matches("\\d", s);
    }
    public boolean regex2(String s) {
        return Pattern.matches("\\d+", s);
    }

//    choose option
    public int option(){
        System.out.print("==> Choose Option[1-4] : ");
        String ch = sc.nextLine();
        if(regex1(ch) == true){
            int z = Integer.parseInt(ch);
            if(z > 4 || z == 0){
                System.out.println("Invalid Number!!!");
                option();
            }else{
                opt = z;
            }
        }else{
            option();
        }
        return opt;
    }
    //  Exit
    public void Exit(){
        System.out.println();
        System.out.println("      Are you sure?      ");
        System.out.println("PRESS [y]Yes or [n]No ???");
        String ans = sc.nextLine();
        if (regex(ans)){
            char[] ch = ans.toCharArray();
            for(char z:ch){
                if(z == 'y'){
                    System.out.println("Turning off System...");
                    System.out.println("GoodBye sir <3");
                    System.exit(0);
                }else if(z == 'n'){
                    System.out.println("Canceled...!!!\n");
                    System.out.println();
                }else{
                    Exit();
                }
            }
        }else{
            Exit();
        }
    }


//==================>> Add - Volunteer <<==================
//    Validate ID,Name,Address
    public int valiID(){
        System.out.print("==> Enter Staff's ID      : ");
        String s = sc.nextLine();
        if(regex1(s) == true){
            iD = Integer.parseInt(s);
        }else{
            valiID();
        }
        return iD;
    }

    public String valiName(){
        System.out.print("==> Enter Staff's Name    : ");
        String s = sc.nextLine();
        if(regex(s) == true){
            namE = s;
        }else{
            valiName();
        }
        return namE;
    }

    public String valiAddress(){
        System.out.print("==> Enter Staff's Address : ");
        String s = sc.nextLine();
        if(regex(s) == true){
            addresS = s;
        }else{
            valiAddress();
        }
        return addresS;
    }

    public void Volun(){
        int id;
        String name,address;
        System.out.println("==========>> Add Volunteer <<==========");
        id = valiID();
        name = valiName();
        address = valiAddress();
        staffMem.add(new Volunteer(id,name,address));
        System.out.println();
        System.out.println("    Volunteer added successfully...");
        System.out.println("**********************************************");

    }

//==================>> Add - SalaryEmployee <<==================

    public double salEmpSalary(){
        System.out.print("==> Enter Staff's Salary  : ");
        String s = sc.nextLine();
        if(regex2(s) == true){
            salarY = Double.parseDouble(s);
        }else{
            salEmpSalary();
        }
        return salarY;
    }

    public double salEmpBonus(){
        System.out.print("==> Enter Staff's Bonus   : ");
        String s = sc.nextLine();
        if(regex2(s) == true){
            bonuS = Double.parseDouble(s);
        }else{
            salEmpBonus();
        }
        return bonuS;
    }

    public void SalaryEmp(){
        int id;
        String name,address;
        double salary,bonus;
        System.out.println("==========>> Add Salary Employee <<==========");
        id = valiID();
        name = valiName();
        address = valiAddress();
        salary = salEmpSalary();
        bonus = salEmpBonus();
        staffMem.add(new salariedEmployee(id,name,address,salary,bonus));
        System.out.println();
        System.out.println("    SalaryEmployee added successfully...");
        System.out.println("**********************************************");
    }

//==================>> Add - HourlyEmployee <<==================

    public int hourEmpWorked(){
        System.out.print("==> Enter Staff's Hours : ");
        String s = sc.nextLine();
        if(regex2(s) == true){
            hourS = Integer.parseInt(s);
        }else{
            hourEmpWorked();
        }
        return hourS;
    }

    public double hourEmpRate(){
        System.out.print("==> Enter Staff's Rate : ");
        String s = sc.nextLine();
        if(regex2(s) == true){
            ratE = Integer.parseInt(s);
        }else{
            hourEmpRate();
        }
        return ratE;
    }

    public void HourlyEmp(){
        int id,hour;
        String name,address;
        double rate;
        System.out.println("==========>> Add Hourly Employee <<==========");
        id = valiID();
        name = valiName();
        address = valiAddress();
        hour = hourEmpWorked();
        rate= hourEmpRate();
        staffMem.add(new hourlyEmployee(id,name,address,hour,rate));
        System.out.println();
        System.out.println("    HourlyEmployee added successfully...");
        System.out.println("**********************************************");
    }

//==============>> Remove <<==============
    public int remID(){
        System.out.print("==> Enter Employee ID to Remove   : ");
        String re = sc.nextLine();
        if(regex1(re)){
            remid = Integer.parseInt(re);
        }else{
            remID();
        }
        return remid;
    }

    public void remove(){
        int count = 0;
        System.out.println("========>> DELETE <<========");
        remid = remID();
        for (staffMember s : staffMem){
            if (s.id == remid){
                count++;
            }
        }
        if (count != 0){
            Iterator i = staffMem.iterator();
            while (i.hasNext()){
                staffMember smi = (staffMember) i.next();
                if (smi.id == remid){
                    i.remove();
                    smi.ToString();
                    System.out.println("");
                    System.out.println("Removed Successfully!!!");
                    System.out.println();
                    show();
                    Menu();
                }
            }
        }else {
            System.out.println();
            System.err.println("ID not found!!! cannot remove Employee!!!");
            System.out.println();
            remove();
        }
    }

//==============>> Edit <<==============
    public int editID(){
        System.out.print("==> Enter Employee ID to Update   : ");
        String ed = sc.nextLine();
        if(regex1(ed)){
            editid = Integer.parseInt(ed);
        }else{
            editID();
        }
        return editid;
    }

    public void edit(){
        int count = 0;
        System.out.println("========>> EDIT INFO <<========");
        editid = editID();
        for (staffMember s : staffMem){
            if (s.id == editid){
                count++;
            }
        }
        if (count != 0) {
            Iterator i = staffMem.iterator();
            while (i.hasNext()) {
                staffMember sm = (staffMember) i.next();
                if (sm.id == editid) {
                    System.out.println();
                    sm.ToString();
                    System.out.println();
                    i.remove();
                    if (sm instanceof salariedEmployee) {
                        String name = valiName();
                        String address = valiAddress();
                        double salary = salEmpSalary();
                        double bonus = salEmpBonus();
                        staffMem.add(new salariedEmployee(editid, name, address, salary, bonus));
                    } else if (sm instanceof hourlyEmployee) {
                        String name = valiName();
                        String address = valiAddress();
                        int hour = hourEmpWorked();
                        double rate = hourEmpRate();
                        staffMem.add(new hourlyEmployee(editid, name, address, hour, rate));
                    } else if (sm instanceof Volunteer) {
                        String name = valiName();
                        String address = valiAddress();
                        staffMem.add(new Volunteer(editid, name, address));
                    }
                    System.out.println();
                    System.out.println("Edit Successfully!!!");
                    show();
                    Menu();
                }
            }
        }else {
            System.out.println();
            System.err.println("ID not found!!! cannot edit Employee!!!");
            System.out.println();
            edit();
        }
    }


    //    Menu
    public void Menu(){
        System.out.println();
        System.out.println("1. Add Employee");
        System.out.println("2. Edit");
        System.out.println("3. Remove");
        System.out.println("4. Exit");
        System.out.println();

        int opt = option();
        switch (opt){
            case 1:
                System.out.println();
                System.out.println("========>> INSERT INFO <<========");
                System.out.println();
                System.out.println("1.Volunteer  2.Hourly-Employee  3.Salaried-Employee    4.Back");
                int o = option();
                switch (o){
                    case 1:
                        System.out.println();
                        Volun();
                        break;
                    case 2:
                        System.out.println();
                        SalaryEmp();
                        break;
                    case 3:
                        System.out.println();
                        HourlyEmp();
                        break;
                    case 4:
                        show();
                        Menu();
                        break;
                }
                break;
            case 2:
                System.out.println();
                edit();
                break;
            case 3:
                System.out.println();
                remove();
                break;
            case 4:
                Exit();
                break;
        }
        show();
        Menu();
    }

    public static void main(String[] args) {
	// write your code here
        Main m = new Main();
        m.show();
        m.Menu();

    }
}

