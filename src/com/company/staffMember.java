package com.company;

//====================>> Staff-Member <<====================
abstract class staffMember {
    //    fields
    protected int id;
    protected String name;
    protected String address;

    //  Getter
    public int getID(){
        return id;
    }
    public String getName(){
        return name;
    }
    public String getAddress(){
        return address;
    }
    //  Setter
    public void setID(int i){
        id = i;
    }
    public void setName(String n){
        name = n;
    }
    public void setAddress(String a){
        address = a;
    }
    //    Constructor
    staffMember(int i, String n, String a){
        id = i;
        name = n;
        address = a;
    }
    //  method
    public void ToString(){
        System.out.println("ID      : " + getID());
        System.out.println("Name    : " + getName());
        System.out.println("Address : " + getAddress());
    }

    public abstract double pay();
}

