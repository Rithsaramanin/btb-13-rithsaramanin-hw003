package com.company;

//====================>> Salaried-Employee <<====================
class salariedEmployee extends staffMember{
    private double salary;
    private double bonus;

    //  Getter
    public double getSalary(){
        return salary;
    }
    public double getBonus(){
        return bonus;
    }
    //  Setter
    public void setSalary(double s){
        salary = s;
    }
    public void setBonus(double s){
        bonus = s;
    }

    salariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    public void ToString(){
        System.out.println("ID      : " + getID());
        System.out.println("Name    : " + getName());
        System.out.println("Address : " + getAddress());
        System.out.println("Salary  : " + getSalary());
        System.out.println("Bonus   : " + getBonus());
        System.out.println("Payment : " + pay());
    }
    @Override
    public double pay() {
        double payment = salary + bonus;
        return payment;
    }
}