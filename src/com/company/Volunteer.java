package com.company;

//====================>> Volunteer <<====================
class Volunteer extends staffMember{
    Volunteer(int id, String name, String address) {
        super(id, name, address);
    }
    public void ToString(){
        System.out.println("ID      : " + getID());
        System.out.println("Name    : " + getName());
        System.out.println("Address : " + getAddress());
    }
    @Override
    public double pay() {
        return 0.0;
    }
}
